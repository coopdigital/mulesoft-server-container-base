#!/usr/bin/env sh

# Calls Mule Agent API to fetch the state of our application
# Returns 0 if application is started, otherwise 1, printing the detected state to stdout

STATE_RESPONSE_FILE=/tmp/liveness
STATE_URL=http://localhost:7777/mule/applications
CURL_OPTS="-s --max-time 2 -w %{http_code} -o $STATE_RESPONSE_FILE"

# clean up from previous run
rm -f $STATE_RESPONSE_FILE

HTTP_CODE=$(curl $CURL_OPTS $STATE_URL)
EXIT_CODE=$?
if [ $HTTP_CODE -eq 0 ]; then 
    echo Failed to connect: $EXIT_CODE
    exit 1
elif [ "$HTTP_CODE" = "000" ]; then
    echo Response: $HTTP_CODE
    exit 1
fi

APP_STATE=$(jq -r '.[0].state' < $STATE_RESPONSE_FILE)
if [ "$APP_STATE" = "STARTED" ]; then
    exit 0
else
    echo $APP_STATE
    exit 1
fi