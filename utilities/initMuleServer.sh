#!/bin/bash
# This script should be invoked by a Kubernetes PostStart hook to handle registering the Runtime container
# as well as adding it into a server group if required. It relies on several environment variables being
# set by Kubernetes. These are documented below.
#
#
# *ANYPOINT_USERNAME: The Anypoint username to log into the platform with
# *ANYPOINT_PASSWORD: The Anypoint password to log into the platform with
# *ANYPOINT_ORG: The name of the organisation within Anypoint to register the server again
# *ANYPOINT_ENV: The environment in Anypoint to register the server against
# *ANYPOINT_HOST: If using PCE the host name of the Platform- do not include the protocol
# *RUNTIME_NAME: The name to register the server as
# *SERVER_GROUP: The name of the server group to create or join this can be null if server group is not used

MULE_BASE="/opt/mule"

LOG_FILE="${MULE_BASE}/logs/anypoint-cli.log"

logit() 
{
	echo "`date +"%Y-%m-%d %T"` [initMuleServer] ${*}"
	echo "`date +"%Y-%m-%d %T"` [initMuleServer] ${*}" >> ${LOG_FILE}
}

initMuleServer(){
	
#Set these into the shell for CLI use.
export ANYPOINT_USERNAME
export ANYPOINT_PASSWORD
export ANYPOINT_ORG
export ANYPOINT_ENV
export RUNTIME_NAME
export SERVER_GROUP
export WEB_PROXY
export SOCKS5_PROXY
export PROXY_HOST
export PROXY_WEB_PORT
export POD_NAME


logit "Using the following values provided by Kubernetes:"
logit "K8S POD_NAME:${POD_NAME}"
logit "Mule Runtime name:${POD_NAME}"
logit "Server Group name:${SERVER_GROUP}"
logit "Anypoint org:${ANYPOINT_ORG}"
logit "Anypoint environment:${ANYPOINT_ENV}"
logit "Anypoint user:${ANYPOINT_USERNAME}"
logit "Using Web Proxy:${WEB_PROXY}"
logit "Using Socks5 Proxy:${SOCKS5_PROXY}"
logit "Using PROXY_HOST:${PROXY_HOST}"
logit "Using PROXY_WEB_PORT:${PROXY_WEB_PORT}"
logit "POD_NAME:${POD_NAME}"





#Get the token for registering the server
ARM_TOKEN="$(anypoint-cli runtime-mgr server token)"
logit "ARM_TOKEN:${ARM_TOKEN}"

REGEX_STR="^([a-zA-Z0-9]*-[a-zA-Z0-9]*){7}$"
logit "ARM Matching expression:${REGEX_STR}"

if echo "$ARM_TOKEN" | grep -Eq $REGEX_STR ; then
   logit "Succesfully got ARM Token, will continue the setup"
else
    logit "Error getting ARM token, exiting initMuleServer"
    exit -1
fi

logit "Using K8S pod name for server"
#RND_RUNTIME_NAME="${RUNTIME_NAME}-$(shuf -i 1000-9999 -n 1)"
RND_RUNTIME_NAME="${POD_NAME}"
logit "POD name for server: ${RND_RUNTIME_NAME}"


#Add the ServerName to a config file for de-registration use
if [ -e ${MULE_BASE}/conf/env_vars ]
then
    logit 'Adding server Name to new env variables file'
    echo "SERVERNAME="${RND_RUNTIME_NAME} > ${MULE_BASE}/conf/env_vars
else
    logit 'Adding server Name to existing env variables file'
    #echo "SERVERNAME="${RND_RUNTIME_NAME} >> ${MULE_BASE}/conf/env_vars
	grep -q "SERVERNAME=" ${MULE_BASE}/conf/env_vars && 
			sed -i "/^SERVERNAME/s/=.*$/=${RND_RUNTIME_NAME}/" ${MULE_BASE}/conf/env_vars || echo "SERVERNAME="${RND_RUNTIME_NAME} >> ${MULE_BASE}/conf/env_vars
fi

#Register the runtime
logit 'Registering server to ARM'
if [ -z "$WEB_PROXY" ]
then
logit "Proxy server not specified registering directly with ARM"
${MULE_BASE}/bin/amc_setup -H $ARM_TOKEN $RND_RUNTIME_NAME 
else
logit "Proxy server specified registering with Proxy with ARM Using Proxy:${WEB_PROXY}"
${MULE_BASE}/bin/amc_setup -H $ARM_TOKEN $RND_RUNTIME_NAME -P $WEB_PROXY
fi


#mule-agent.yml has been created, now enable the rest client for heartbeat
if [ -e ${MULE_BASE}/conf/mule-agent.yml ]
then
	logit 'Enabling REST client..'
	sed -i '/^ *rest.agent.transport:/,/^ *[^:]*:/s/enabled: false/enabled: true\n    port: 7777/'  ${MULE_BASE}/conf/mule-agent.yml

	logit "Configuring proxy for API manager"
	echo "wrapper.java.additional.22=-Danypoint.platform.proxy_host=$PROXY_HOST" >> ${MULE_BASE}/conf/wrapper.conf
	echo "wrapper.java.additional.23=-Danypoint.platform.proxy_port=$PROXY_WEB_PORT" >> ${MULE_BASE}/conf/wrapper.conf
	
	logit "Adding header_injection property for Anypoint Monitoring"
	echo "wrapper.java.additional.24=-Danypoint.platform.config.analytics.agent.header_injection.disabled=false" >> ${MULE_BASE}/conf/wrapper.conf
	
	logit 'Configuring MuleSoft Anypoint Monitoring..'
	CLI_LOG=$( echo N | $MULE_BASE/am/bin/install -p socks5://$SOCKS5_PROXY;)
	logit "AM Configured: ${CLI_LOG}"
	logit 'Done configuring all the steps'
else
	logit 'mule-agent.yml not found after amc_setup, skipping Additional proxy config'
fi

logit 'Done with initMuleServer will continue to mule server boot process'

}

#register mule server
initMuleServer

#Start the mule server
# This will exec the CMD from your Dockerfile, i.e. "mule start"
logit "Starting the mule server now with command: $@"
exec "$@"


