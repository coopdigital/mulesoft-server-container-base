#!/bin/bash
# This script should be invoked by a Kubernetes PreExit hook to handle removing the Runtime container
# as well as deleting it from a server group if required and also remove the server group if it is left
# empty. It relies on several environment variables being set by Kubernetes. These are documented below.
#
# *ANYPOINT_USERNAME: The Anypoint username to log into the platform with
# *ANYPOINT_PASSWORD: The Anypoint password to log into the platform with
# *ANYPOINT_ORG: The name of the organisation within Anypoint to register the server again
# *ANYPOINT_ENV: The environment in Anypoint to register the server against
# *ANYPOINT_HOST: If using PCE the host name of the Platform- do not include the protocol
# *RUNTIME_NAME: The name to register the server as
# *SERVER_GROUP: The name of the server group to create or join this can be null if server group is not used





MULE_BASE="/opt/mule"

LOG_FILE="${MULE_BASE}/logs/anypoint-cli.log"

logit() 
{
	echo "`date +"%Y-%m-%d %T"` [anypointCliUnRegistration] ${*}"
	echo "`date +"%Y-%m-%d %T"` [anypointCliUnRegistration] ${*}" >> ${LOG_FILE}
}

trim-cli()
{
cat $1 | sed 's/ *$//' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g"
}

logit "Using the following values provided by Kubernetes:"
logit "K8S POD_NAME:${POD_NAME}"
logit "Mule Runtime name:${POD_NAME}"
logit "Server Group name:${SERVER_GROUP}"
logit "Anypoint org:${ANYPOINT_ORG}"
logit "Anypoint environment:${ANYPOINT_ENV}"
logit "Anypoint user:${ANYPOINT_USERNAME}"
logit "Using Web Proxy:${WEB_PROXY}"
logit "Using Socks5 Proxy:${SOCKS5_PROXY}"
logit "Using PROXY_HOST:${PROXY_HOST}"
logit "Using PROXY_WEB_PORT:${PROXY_WEB_PORT}"
logit 'Shutdown detected. Attempting to deregister the runtime with ARM'

#Attempt to get the serverID from the env_vars file created in the registration process.
if [ -e ${MULE_BASE}/conf/env_vars ]
then
	echo 'Finding Values stored in env_vars'
	SERVERID=$(awk -F= -v x="SERVERID" '$1==x{print $2}' ${MULE_BASE}/conf/env_vars)
	#RND_RUNTIME_NAME=$(awk -F= -v x="SERVERNAME" '$1==x{print $2}' ${MULE_BASE}/conf/env_vars)
	RND_RUNTIME_NAME="${POD_NAME}"
	SERVERGRPID=$(awk -F= -v x="SERVERGRPID" '$1==x{print $2}' ${MULE_BASE}/conf/env_vars)
	
    logit "ServerID=${SERVERID}"
	logit "RND_RUNTIME_NAME=${RND_RUNTIME_NAME}"
	logit "SERVERGRPID=${SERVERGRPID}"
	
	#Check if able to get SERVERID from env_vars else get it from cli
	if [ -z $SERVERID ]
	then
		logit 'Using anypoint-cli to get server ID based on runtime name set as a variable in K8S.'
		SERVERID=$(anypoint-cli runtime-mgr server list -f ID,Name -o text | trim-cli | grep "$RND_RUNTIME_NAME\$" | awk '{print $1}'  )
		logit "ServerID=${SERVERID}"
	fi
	
	
else
    logit 'Env var file not detected. This script has already been invoked exiting now.'
    exit -1
fi



#Check if SERVER_GROUP field is present, if it is remove the server from the server group and check if
#that was the last member of the group. If yes delete the server group as well.
#if server group environment variable is not set just delete the server
if [ -z ${SERVER_GROUP} ]
then
    logit 'Server group environment variable is not set. Deleting just the server from ARM'
    anypoint-cli runtime-mgr server delete ${SERVERID}
    logit 'Deleted server now removing env_vars file'
    rm ${MULE_BASE}/conf/env_vars
else
  logit 'The server belongs to a server group. Finding the server group ID'
  #SERVERGRPID=$(anypoint-cli runtime-mgr serverGroup list -f ID,Name -o text | grep $SERVER_GROUP | awk '{print $1}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" )
  logit "Server group ID is:${SERVERGRPID} for server group:$SERVER_GROUP"

  logit "Removing server:${RND_RUNTIME_NAME} from ${SERVER_GROUP}"
  anypoint-cli runtime-mgr serverGroup remove server ${SERVERGRPID} ${SERVERID}

  logit "Deleting server"
  anypoint-cli runtime-mgr server delete ${SERVERID}

  logit "Checking if any servers remain in the server group"
  serverGroupResponse=$(anypoint-cli runtime-mgr serverGroup describe ${SERVERGRPID})
  
  logit "Server Group Response: ${serverGroupResponse}"
  case "${serverGroupResponse}" in
  *EMPTY* )
			echo "No more servers exist deleting the server group"
			anypoint-cli runtime-mgr serverGroup delete ${SERVERGRPID}
			;;
   * 	  )
			logit "Server group is not empty, skipping deletion"
			;;
   esac
  
  echo 'Finished removing the server in ARM now removing the env vars file to avoid duplication'
  rm ${MULE_BASE}/conf/env_vars
  rm ${MULE_BASE}/conf/mule-agent.yml

fi
