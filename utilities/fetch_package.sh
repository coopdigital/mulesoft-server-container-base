#!/usr/bin/env bash

PACKAGE=$1
OUTPUT_DIR=$2

wget  --http-user=${NEXUS_USERNAME} --http-password=${NEXUS_PASSWORD} ${PACKAGE} -P ${OUTPUT_DIR}
