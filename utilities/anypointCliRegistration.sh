#!/bin/bash
# This script should be invoked by a Kubernetes PostStart hook to handle registering the Runtime container
# as well as adding it into a server group if required. It relies on several environment variables being
# set by Kubernetes. These are documented below.
#
#
# *ANYPOINT_USERNAME: The Anypoint username to log into the platform with
# *ANYPOINT_PASSWORD: The Anypoint password to log into the platform with
# *ANYPOINT_ORG: The name of the organisation within Anypoint to register the server again
# *ANYPOINT_ENV: The environment in Anypoint to register the server against
# *ANYPOINT_HOST: If using PCE the host name of the Platform- do not include the protocol
# *RUNTIME_NAME: The name to register the server as
# *SERVER_GROUP: The name of the server group to create or join this can be null if server group is not used


	



MULE_BASE="/opt/mule"

LOG_FILE="${MULE_BASE}/logs/anypoint-cli.log"

logit() 
{
	echo "`date +"%Y-%m-%d %T"` [anypointCliRegistration] ${*}"
	echo "`date +"%Y-%m-%d %T"` [anypointCliRegistration] ${*}" >> ${LOG_FILE}
}

trim-cli()
{
cat $1 | sed 's/ *$//' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g"
}

logit "Using the following values provided by Kubernetes:"
logit "K8S POD_NAME:${POD_NAME}"
logit "Mule Runtime name:${POD_NAME}"
logit "Server Group name:${SERVER_GROUP}"
logit "Anypoint org:${ANYPOINT_ORG}"
logit "Anypoint environment:${ANYPOINT_ENV}"
logit "Anypoint user:${ANYPOINT_USERNAME}"
logit "POD_NAME:${POD_NAME}"

logit "Running PostStart hook for server registration"
#Check for the precense of env_vars file. If this already exists this script has already been invoked so exit to avoid duplication
#Add the serverID to a config file for de-registration use

STATUS=$(${MULE_BASE}/bin/mule status)
logit "Checking Server status in pod: ${STATUS}"

until [[  $STATUS =~ "running: PID" ]];
do
	 logit "mule has not started, going to sleep"
     sleep 30
	 STATUS=$(${MULE_BASE}/bin/mule status)
done

logit "Server is up, will start processing server registration"
		
if [ -e ${MULE_BASE}/conf/env_vars ]
then
    logit 'Finding Values stored in env_vars'
	#RND_RUNTIME_NAME=$(awk -F= -v x="SERVERNAME" '$1==x{print $2}' ${MULE_BASE}/conf/env_vars)
	RND_RUNTIME_NAME="${POD_NAME}"
	logit "RANDOM SERVERNAME=${RND_RUNTIME_NAME}"
	
	#Get the server id for the newly registered server.
	#the sed command is to remove ANSI characters returned from the CLI
	SERVERID=$(anypoint-cli runtime-mgr server list -f ID,Name -o text | trim-cli | grep "$RND_RUNTIME_NAME\$" | awk '{print $1}' )
	logit "ServerID: ${SERVERID}"
		
	#Check the server was correctly registered if not throw an exception
	if [ -z $SERVERID ]
	then
		logit "Could not retreive Server ID, might be an issue creating server"
		exit -1
	fi

	logit 'Adding server ID to existing env variables file'
	
	grep -q "SERVERID=" ${MULE_BASE}/conf/env_vars && 
    sed -i "/^SERVERID/s/=.*$/=${SERVERID}/" ${MULE_BASE}/conf/env_vars || echo "SERVERID="${SERVERID} >> ${MULE_BASE}/conf/env_vars

	if [ -z ${SERVER_GROUP} ]
	then
		logit 'Server group environment variable is not set. Not creating or joining a server group'
	else
		#check if ARM is updated with the server status to running and then move the server to group
		logit "Checking server status in ARM"
		
		SERVER_STATUS=$(anypoint-cli runtime-mgr server describe -f Status -o text $SERVERID | awk '{print $2}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
		logit "ARM Server Status : ${SERVER_STATUS}"
		
		until  [[  $SERVER_STATUS =~ "RUNNING" ]];
		do
		 logit "Server is not in running status in ARM, going to sleep for a while"
		 sleep 10
		 SERVER_STATUS=$(anypoint-cli runtime-mgr server describe -f Status -o text $SERVERID | awk '{print $2}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
		 logit "ARM Server Status : ${SERVER_STATUS}"
		done
		
		logit 'looks like server is up in ARM, doing server movement'
		
		logit 'Joining a server group. Checking to see if the server group exists'
		SERVERGRPID=$(anypoint-cli runtime-mgr serverGroup list -f ID,Name -o text | trim-cli | grep "$SERVER_GROUP\$" | awk '{print $1}' )
		logit "Server Group ID retrieved:${SERVERGRPID} from Server Group: ${SERVER_GROUP}"

		if [ -z $SERVERGRPID ]
		then
		  logit 'No server groupID currently exists- creating the server group'
	  	  CLI_LOG=$(anypoint-cli runtime-mgr serverGroup create "${SERVER_GROUP}")
		  logit "Create Group: ${CLI_LOG}"
		  
		  SERVERGRPID=$(anypoint-cli runtime-mgr serverGroup list -f ID,Name -o text | trim-cli | grep "$SERVER_GROUP\$" | awk '{print $1}' )
		  logit "SERVERGRPID: ${SERVERGRPID}"
		  
	  	  CLI_LOG=$(anypoint-cli runtime-mgr serverGroup add server $SERVERGRPID $SERVERID)
		  logit "Add Server to Group: ${CLI_LOG}"
		  
		  grep -q "SERVERGRPID=" ${MULE_BASE}/conf/env_vars && 
			sed -i "/^SERVERGRPID/s/=.*$/=${SERVERGRPID}/" ${MULE_BASE}/conf/env_vars || echo "SERVERGRPID="${SERVERGRPID} >> ${MULE_BASE}/conf/env_vars
		  logit "Created Server Group with groupID:${SERVERGRPID} for group:${SERVER_GROUP}"
		else
		  logit 'The Server Group already exists adding server to it'
		  CLI_LOG=$(anypoint-cli runtime-mgr serverGroup add server $SERVERGRPID $SERVERID)
		  logit "Add Server to Group: ${CLI_LOG}"
		   
		 grep -q "SERVERGRPID=" ${MULE_BASE}/conf/env_vars && 
			sed -i "/^SERVERGRPID/s/=.*$/=${SERVERGRPID}/" ${MULE_BASE}/conf/env_vars || echo "SERVERGRPID="${SERVERGRPID} >> ${MULE_BASE}/conf/env_vars
		fi
	fi
	
	
	# Self contained API deployment process starts here
	if [[ $DEPLOY_API == "Y" ]];
	then
		logit 'Self contained deployed flag is set, checking to deploy the API'
		cd /api/package
		API_PACKAGE=$(ls $SERVER_GROUP*.jar | sort -t- -k2 -V -r | head -1)
		logit "Latest version of the API picked for deployment  ${API_PACKAGE}"
		
		APP_STATUS_STR=$(anypoint-cli runtime-mgr standalone-application list -f ID,Name,Status -o text | trim-cli | grep "$SERVER_GROUP\$"  )
		logit "Retrieved App Status for API ${SERVER_GROUP} is:${APP_STATUS_STR}"
		APP_STATUS=$( echo $APP_STATUS_STR | awk '{print $3}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
		ARM_ID=$( echo $APP_STATUS_STR | awk '{print $1}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
		logit "Retrieved App Status for API ${SERVER_GROUP} ARM ID: ${ARM_ID} is: ${APP_STATUS}"
		if [ -z ${APP_STATUS} ]
		then
			logit "API Status is empty, assuming API is not deployed, will try to deploy"
			logit "In case of race around condition when multiple pods are coming up,deployment for one of them goes sucessful and for the others the API deployment fails but ARM takes care of deploying it to all the servers in the group"
			
			MAVEN_OUT=$(mvn -s pod_ci_settings.xml -f pom.xml mule:deploy -Dmule.artifact=$API_PACKAGE -P $ANYPOINT_ENV -Druntime.property=$ENCRYPTION_KEY -Ddep.skipDeploymentVerification=true )
			logit "maven output: ${MAVEN_OUT}"
		elif [[ $APP_STATUS == "STARTED" || $APP_STATUS == "STOPPED" ]];
		then
			logit "The API is already started/stopped"
			ARM_SHA1SUM=$(anypoint-cli runtime-mgr standalone-application describe -f "File Checksum" -o text $ARM_ID | awk '{print $3}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
			logit "ARM Sha1Sum of the API: ${ARM_SHA1SUM}"
			
			LOCAL_SHA1SUM=$(sha1sum $API_PACKAGE | awk '{print $1}')
			logit "LOCAL Sha1Sum of the API: ${LOCAL_SHA1SUM}"
			
			if [ "$LOCAL_SHA1SUM" == "$ARM_SHA1SUM" ]; 
			then
				logit "SHA1SUM matches for the deployed and local API, ARM will take care of the deploying the app on the server"
			else
				logit "SHA1SUM mismatch, will redeploy the API"
				MAVEN_OUT=$(mvn -s pod_ci_settings.xml -f pom.xml mule:deploy -Dmule.artifact=$API_PACKAGE -P $ANYPOINT_ENV -Druntime.property=$ENCRYPTION_KEY -Ddep.skipDeploymentVerification=true )
				logit "maven output: ${MAVEN_OUT}"
			fi			
		else
			logit "No matching conditions of deployment logic"
		fi
	else
		logit "Self contained deployed flag if off, skipping API deployment: ${DEPLOY_API}"
	fi
	
	
else
	logit "Can not find the env_vars to process further"
	exit -1
fi

# Start Anypoint Monitoring
logit 'Starting AM agent'
CLI_LOG=$($MULE_BASE/am/bin/setup)
logit "AM agent log: ${CLI_LOG}"
