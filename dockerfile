FROM adoptopenjdk/openjdk11
MAINTAINER              Co-Operative Bank CoE API team

# Build arguements
# NEXUS credentials are the Mulesoft EE Nexus repo
# NEXUS url can be overriden with internal corporate copy/proxy of repo
ARG                     NEXUS_USERNAME=cooperative.bank.nexus
ARG                     NEXUS_PASSWORD=MIeckhiK
ARG                     MULE_VERSION=4.3.0-20210304
ARG                     MULE_AGENT_VERSION=2.4.18
ARG                     MULE_AM_VERSION=2.4.2.0
ARG                     NEXUS_EE_RELEASES=https://repository.mulesoft.org/nexus/content/repositories/releases-ee/

# configuration for connecting to API Manager (defaults are Cloudhub values)
ENV                     PLATFORM_BASE=https://anypoint.mulesoft.com/apiplatform
ENV                     CORE_SERVICES_BASE=https://anypoint.mulesoft.com/accounts
ENV                     ANALYTCIS_BASE=
ENV                     CONTRACTS_BASE=https://anypoint.mulesoft.com/apigateway/ccs
ENV                     ONPREM=false

#RUN apk add --no-cache bash gawk sed grep bc coreutils jq nodejs
RUN                     apt-get update && \
	                      apt-get --assume-yes install curl tar bash procps jq && \
	                      apt-get clean && \
	                      rm -rf /var/lib/apt/lists/*

#RUN npm commands to install anypoint CLI
RUN 									 	curl -sL https://deb.nodesource.com/setup_12.x | bash  && \
											 	apt-get install -y nodejs vim git wget unzip && \
 							 					npm install -g anypoint-cli@latest
												
# Downloading and installing Maven
# 1- Define a constant with the version of maven you want to install
ARG MAVEN_VERSION=3.6.3

# 2- Define a constant with the working directory
ARG USER_HOME_DIR="/root"

# 3- Define the SHA key to validate the maven download
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0

# 4- Define the URL where maven can be downloaded from
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

# 5- Create the directories, download maven, validate the download, install it, remove downloaded file and set links
RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && echo "Downlaoding maven" \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  \
  && echo "Checking download hash" \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  \
  && echo "Unziping maven" \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  \
  && echo "Cleaning and setting links" \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

# 6- Define environmental variables required by Maven, like Maven_Home directory and where the maven repo is located
ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

#Add fetch package shell script
COPY										utilities/fetch_package.sh /

#Install mule
WORKDIR                 /opt/
RUN                     /fetch_package.sh "${NEXUS_EE_RELEASES}/com/mulesoft/mule/distributions/mule-ee-distribution-standalone/${MULE_VERSION}/mule-ee-distribution-standalone-${MULE_VERSION}.tar.gz" "/opt" "${NEXUS_USERNAME}" "${NEXUS_PASSWORD}" && \
                        tar -xvf mule-ee-distribution-standalone*.tar.gz && \
                        rm mule-ee-distribution-standalone*.tar.gz && \
                        rm -rf /opt/mule && \
                        ln -s /opt/mule-enterprise-standalone-$MULE_VERSION /opt/mule

# add the additional config required for running in a container
#RUN                     echo '#include.required /opt/mule/conf/wrapper-docker.conf' >> /opt/mule/conf/wrapper.conf
#RUN                     sed -i '/XX\:\(Max\)\{0,1\}NewSize/d' /opt/mule/conf/wrapper.conf && \
#												sed -i '/XX\:\(Max\)\{0,1\}PermSize/d' /opt/mule/conf/wrapper.conf
#COPY 					conf/wrapper-docker.conf /opt/mule/conf/wrapper-docker.conf

#Add configured log4j from the conf folder
#COPY 										conf/log4j2.xml /opt/mule/conf/

#Copy the Mule license
COPY 										conf/license.lic /opt/mule/bin/

#Update Mule Agent from Mule repo
#RUN                     /fetch_package.sh "${NEXUS_EE_RELEASES}/com/mulesoft/agent/agent-setup/${MULE_AGENT_VERSION}/agent-setup-${MULE_AGENT_VERSION}.zip" "/opt" "${NEXUS_USERNAME}" "${NEXUS_PASSWORD}"
# Reverted to downloaded mule-agent zips as Mule is not maintaining the maven download repo
COPY					packages/agent-setup-${MULE_AGENT_VERSION}.zip /opt/
RUN						unzip -o agent-setup-${MULE_AGENT_VERSION}.zip -d /opt/mule/bin && \
						rm agent-setup-${MULE_AGENT_VERSION}.zip

#Copy the Mule Anypoint Monitoring package
RUN                     /fetch_package.sh "https://s3.amazonaws.com/cdn-anypoint-prod/artifacts/monitoring-center-ui/app/hybrid/am-${MULE_AM_VERSION}.zip" "/opt" "" ""
RUN						unzip am-${MULE_AM_VERSION}.zip -d /opt/mule/  && \
						rm am-${MULE_AM_VERSION}.zip

#Add anypointCliRegistration.sh and anypointCliUnregistration.sh to bin to be invoked by kubernetes
COPY                    utilities/* /opt/mule/bin/
RUN 					chmod +x /opt/mule/bin/anypointCliUnregistration.sh
RUN 					chmod +x /opt/mule/bin/liveness-probe.sh
RUN 					chmod +x /opt/mule/bin/readiness-probe.sh
RUN 					chmod +x /opt/mule/bin/initMuleServer.sh
RUN 					chmod +x /opt/mule/bin/anypointCliRegistration.sh
RUN 					chmod +x /opt/mule/bin/cleanup-arm-env.sh

# Expose Test Service port
EXPOSE                  8081

# Inject proxy and exela certs into base container image
WORKDIR					/opt/workdir
ARG						CERT="certificate.crt"
COPY 					$CERT /opt/workdir
COPY 					utilities/installcerts.sh /opt/workdir
RUN 					chmod +x /opt/workdir/installcerts.sh
RUN 					./installcerts.sh $CERT changeit

# Environment and execution:
ENV                     MULE_BASE /opt/mule
WORKDIR                 /opt/mule/bin/

# Install Mule license
RUN						./mule -installLicense license.lic

# Update Mule Agent, this step is not needed for fresh unregistered server
#RUN						./amc_setup -U

# Increast JVM heapspace
RUN						sed -i "/^wrapper.java.additional.7/s/=.*$/=-XX:MaxMetaspaceSize=512m/" /opt/mule/conf/wrapper.conf
RUN						sed -i "/^wrapper.java.additional.8/s/=.*$/=-XX:MetaspaceSize=256m/" /opt/mule/conf/wrapper.conf

# CLIENTID and CLIENTSECRET mut be supplied at startup if the gateway is to be managed
ENTRYPOINT ["/opt/mule/bin/initMuleServer.sh"]
CMD ["/opt/mule/bin/mule", "console"]
