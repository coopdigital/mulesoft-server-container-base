# mule-docker-arm-integration-kubernetes

A docker image that is based on a java image and layers the Mule runtime plus other required additions to support the Mule runtime. A Mule application zip can be layered on top of this base image to create an image that contains both runtime and app. 

## JVM Memory settings 
This base image uses the [Java SE support for Containers/CGroups](https://blogs.oracle.com/java-platform-group/java-se-support-for-docker-cpu-and-memory-limits) to calculate the memory to use in the container. You can specify JVM_HEAP_FRACTION as an environment variable when running the docker image (see below).  

## Build the Base Image
Build the mule base image

`docker build -t mule-base-image:LATEST .`

## Build the App Image  
Build the app image by layering the app zip on the base image. This can be acheived with a simple docker file, for example: 

`FROM mule-base-image:LATEST`

`COPY ./target/<mule_app>.zip /opt/mule/apps/`

Then build the app image  

`docker build -t app-image-name:LATEST .`

## Run the App image 
Once the image has been created that contains the app and the runtime it can be run as follows

`docker run -d -p 8081:8081 -e CLIENTSECRET=xxxxxx -e CLIENTID=xxxxxxx app-image-name:LATEST`

Note that the client_id and secret required to connect to API Manager can be specified as environment variables. 

For the JVM to be be able to calculate the correct amount of memory to assign to the heap, memory limits must be specified for the container. Additionally the fraction of the container memory to use for the heap can be specified as an environment variable. A fraction of 2 will assign 50% of the containers memory to the heap. 

`docker run -d -p 8081:8081 -m 1g -e JVM_HEAP_FRACTION=2 app-image-name:LATEST`

### On Prem API Manager 

To connect to Anypoint PCE you must supply additional env vars

`docker run -d -p 8081:8081 -e CLIENTSECRET=xxxxxx -e CLIENTID=xxxxxxx -e PLATFORM_BASE=https://pce.company.com/apiplatform -e CORE_SERVICES_BASE=https://pce.company.com/accounts -e CONTRACTS_BASE=https://pce.company.com/apigateway/ccs -e ONPREM=true app-image-name:LATEST`




